package com.glearning;

public class TestClass {

    public static void main(String[] args) {
        System.out.println(new MyParent().getName());
    }
}
interface MyInterface {
    String name = "Interface Variable";
    String getName();
}

abstract class MyAbstractClass implements MyInterface{
    String name = "Abstract Class Variable";
}

class MyParent extends MyAbstractClass{
    @Override
    public String getName() {
        return MyInterface.name;
    }
}





