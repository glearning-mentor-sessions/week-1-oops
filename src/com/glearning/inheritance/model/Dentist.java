package com.glearning.inheritance.model;

public class Dentist extends Doctor{

    @Override
    public void treatPatient() {
        System.out.println("Treating tooth ailments");
    }
}
