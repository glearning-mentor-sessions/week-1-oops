package com.glearning.inheritance.model;

public abstract class Doctor {
    private int experience;
    private String name;

    public abstract void treatPatient();
}
