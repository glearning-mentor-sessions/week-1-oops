package com.glearning.inheritance.model;

public class KneeSurgeon extends Orthopedician {
    public void kneeSurgery() {
        System.out.println("Performing knee surgery!!");
    }
}
