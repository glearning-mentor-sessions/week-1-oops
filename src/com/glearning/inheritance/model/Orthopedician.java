package com.glearning.inheritance.model;

public class Orthopedician extends Doctor {

    public void treatPatient(){
        conductCTScan();
        conductXRay();
    }

    public void conductCTScan(){
        System.out.println("Conducting CT-scan");
    }
    public void conductXRay(){
        System.out.println("Conducting X-Ray");
    }
}
