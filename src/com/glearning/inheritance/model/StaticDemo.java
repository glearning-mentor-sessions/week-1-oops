package com.glearning.inheritance.model;

import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.IntStream;

public class StaticDemo {

    private static Comparator<User> ageComparatorASC = (user1,  user2) -> user1.getAge() - user2.getAge();

    private static Comparator<User> ageComparatorDESC = ageComparatorASC.reversed();



    public static void main(String[] args) {
        List<User> users = List.of(
                new User("vinod", 22, 35000),
                new User("harish", 23, 55000),
                new User("akash", 35, 75000),
                new User("meena", 37, 65000)
                );
        List<Integer> ages = List.of(2,44,22,12,56,33,23,25);
        Integer.max(34, 56);
        Integer.parseInt("435");

        TreeSet<User> sortedUsers = new TreeSet<>(ageComparatorASC);
        sortedUsers.addAll(users);
        sortedUsers.forEach(user -> System.out.println(user.getFirstName()));
    }
}



class AgeComparatorDESC implements Comparator<User>{

    @Override
    public int compare(User user1, User user2) {
        return user2.getAge() - user1.getAge();
    }
}

class NameComparatorASC implements Comparator<User>{

    @Override
    public int compare(User user1, User user2) {
        return user1.getFirstName().compareTo(user2.getFirstName());
    }
}
class NameComparatorDESC implements Comparator<User>{

    @Override
    public int compare(User user1, User user2) {
        return user2.getFirstName().compareTo(user1.getFirstName());
    }
}


class User implements Comparable<User> {

    private String firstName;
    private int age;
    private double salary;

    public User(String firstName, int age, double salary) {
        this.firstName = firstName;
        this.age = age;
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public int compareTo(User another) {
        return this.age - another.age;
    }
}
