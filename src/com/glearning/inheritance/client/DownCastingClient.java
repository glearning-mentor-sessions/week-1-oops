package com.glearning.inheritance.client;

import com.glearning.inheritance.model.Dentist;
import com.glearning.inheritance.model.Doctor;
import com.glearning.inheritance.model.Orthopedician;

public class DownCastingClient {
    public static void main(String[] args) {
        Doctor doctor = new Dentist();

       // doctor.treatPatient();
        if (doctor instanceof Orthopedician) {
            ((Orthopedician) doctor).conductXRay();
        }
    }
}
