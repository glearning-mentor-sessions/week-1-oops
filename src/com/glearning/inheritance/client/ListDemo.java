package com.glearning.inheritance.client;

import java.util.*;

public class ListDemo {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("one", "two", "three", "four");
        List<String> list2 = new ArrayList<>();
        List<String> list3 = new LinkedList<>();
        Set<Integer> numbers =  new HashSet<>();
    }
}
