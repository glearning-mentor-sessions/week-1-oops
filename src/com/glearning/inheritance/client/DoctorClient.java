package com.glearning.inheritance.client;

import com.glearning.inheritance.model.Dentist;
import com.glearning.inheritance.model.Doctor;
import com.glearning.inheritance.model.Orthopedician;
import com.glearning.inheritance.model.Padeitric;

import javax.print.Doc;
import java.util.Scanner;

public class DoctorClient {

    public static void main(String[] args) {
        //LHS = RHS (is a)
        //Doctor doctor1 = new Doctor();
        /*Doctor doctor2 = new Orthopedician();
        Doctor doctor3 = new Padeitric();
        Orthopedician doctor6 = new Orthopedician();
        Padeitric doctor7 = new Padeitric();
        Doctor doctor9 = new Dentist();
*/
        //Orthopedician doctor4 = new Padeitric();
        //Orthopedician doctor5 = new Doctor();

        //the compiler can only call methods that are declared using the reference
        //doctor1.treatPatient();
        //doctor1.treatPatient();
       /* System.out.println("---------------------------");
        doctor2.treatPatient();
        System.out.println("---------------------------");
        doctor3.treatPatient();
        System.out.println("---------------------------");
        doctor9.treatPatient();*/

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please select an option");
        System.out.println( " 1 -> Ortho");
        System.out.println( " 2 -> Dentist");
        System.out.println( " 3 -> Padietric");

        Doctor doctor = null;
        int option = scanner.nextInt();
        switch (option){
            case 1:
                doctor = new Orthopedician();
                break;
            case 2:
                doctor = new Dentist();
                break;
            case 3:
                doctor = new Padeitric();
                break;
            default:
                doctor = new Orthopedician();
                break;
        }

        if (doctor != null){
            doctor.treatPatient();
        }
        scanner.close();
    }
}
