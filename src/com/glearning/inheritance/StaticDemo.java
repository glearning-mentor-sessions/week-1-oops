package com.glearning.inheritance;

public class StaticDemo {

    public static void main(String[] args) {
        SavingsAccount account1 = new SavingsAccount("Praveen", 1_25_874);
        SavingsAccount account2 = new SavingsAccount("Vinood", 2_52_564);
        SavingsAccount account3 = new SavingsAccount("Hari", 2_52_564);

        System.out.println("Account id : "+ account1.getAccountId());
        System.out.println("Account id : "+ account2.getAccountId());

        System.out.println(" Number of accounts created : "+ (SavingsAccount.getCounter() - 1000));
        System.out.println(" Number of accounts created : "+ (SavingsAccount.getCounter() - 1000));

    }
}

class SavingsAccount {

    private String name;
    private double balance;
    private static long counter = 1000;
    private long accountId;

    public SavingsAccount(String name, double balance) {
        this.name = name;
        this.balance = balance;
        this.accountId = ++ counter;
    }

    public static long getCounter(){
        return counter;
    }

    public long getAccountId() {
        return accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "SavingsAccount{" +
                "name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }
}
