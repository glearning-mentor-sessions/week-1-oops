package com.glearning.inheritance.contract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        PaymentGateway gateway = new PhonePay();

        gateway.transfer("Pradeep", " Vinod", 200, " thanks");

        Recharge recharge = new PhonePay();
        recharge.rechagePrepaid("9874587878", 400);

        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);

        List<Integer> newNumbers = Arrays.asList(1,2,3,4,5);

        List<Integer> numbersWithList = List.of(1,2,3,4,5,6);



    }
}
