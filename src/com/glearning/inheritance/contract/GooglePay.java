package com.glearning.inheritance.contract;

public class GooglePay implements PaymentGateway {
    @Override
    public boolean transfer(String from, String to, double amount, String notes) {
        System.out.println(" Transfer of "+ amount+ " from "+ from + " to "+ to + " notes: "+ notes+ " using Google Pay");
        System.out.println(" You have earned "+ (10/100 * amount)+ " as cashback");
        return true;
    }
}
