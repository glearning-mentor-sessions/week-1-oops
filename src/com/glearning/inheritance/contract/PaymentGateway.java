package com.glearning.inheritance.contract;

@FunctionalInterface
public interface PaymentGateway {
    boolean transfer(String from, String to, double amount, String notes);
    default double currencyConversion(String from, String to, double amount){
        return  ((10/100) * amount);
    }
}
