package com.glearning.inheritance.contract;

public class PhonePay implements PaymentGateway, Recharge {
    @Override
    public boolean transfer(String from, String to, double amount, String notes) {
        System.out.println(" Transfer of "+ amount+ " from "+ from + " to "+ to + " notes: "+ notes+ " using Phone Pay");
        System.out.println(" You have earned "+ (20/100 * amount)+ " as reward points");
        return true;
    }

    @Override
    public void rechagePrepaid(String phoneNumber, double amount) {
        System.out.println("Recharge of "+ phoneNumber+ " with amount "+ amount);
    }
}
