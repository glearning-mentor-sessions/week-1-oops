package com.glearning.inheritance.contract;

public interface Recharge {
    void rechagePrepaid(String phoneNumber, double amount);
}
